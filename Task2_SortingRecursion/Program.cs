﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2_SortingRecursion
{
    class Program
    {
        static void Main(string[] args)
        {
            var array =  new int[] { 4, 2, 8, 1, 3, 7, 6, 5 };  //заданный массив для сортировки
            quickSort(array, 0, array.Length - 1);              //сортировка массива
            for (int i = 0; i < array.Length; i++)              //вывод массива на консоль
            {
                Console.WriteLine(string.Format("[{0}] = {1}", i, array[i]));
            }
            Console.ReadLine();
        }

        public static void quickSort(int[] array, int first, int last)
        {
            int temp;                                       // временная переменная
            int x = array[first + (last - first) / 2];      //средний элмент в текущем подмассиве. Запись эквивалентна (l+r)/2, но не вызввает StackOverflowException 
            int i = first;                                  //i - индекс первого элемента
            int j = last;                                   //j- индекс последнего элемента массива

            while (i <= j)                  // шагаем по массиву
            {
                while (array[i] < x) i++;   //проход по элементам слева от х, если элемент меньше х - переход к следующему.
                while (array[j] > x) j--;   //проход по элементам справа от х, если элемент больше х - переход к следующему.

                if (i <= j)                 //если есть не раскиданные элементы - меняем их местами
                {
                    temp = array[i];        //запоминаем элемент с меньшим индексом во вспогательную переменную
                    array[i] = array[j];    //на его место записываем элемент с большим индексом
                    array[j] = temp;        //значение вспомогательной переменной записываем в элемент с большим индексом
                                            //т.е. элементы поменялись местами.
                    i++;                
                    j--;                    //переход к следующим элементам
                }
            }
            if (i < last)
            {
                quickSort(array, i, last);     //рекурсивный вызов для левого подмассива, если это требуется
                Console.WriteLine(string.Format("quickSort({0}, {1}, {2})", array.ToString(), i, last)); //вывод параметров на консоль
            }
            if (first < j)
            {
                quickSort(array, first, j);     //рекурсивный вызов для правого подмассива, если это требуется
                Console.WriteLine(string.Format("quickSort({0}, {1}, {2})", array.ToString(), first, j)); //вывод параметров на консоль
            }
            Console.WriteLine();
        }

        
    }
}

